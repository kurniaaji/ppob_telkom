/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BILLER.SW;
import ppob_telkom.Settings;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.persistence.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.*;
import model.Inboxes;
import model.Outboxes;
import model.TokenUnsolds;
import model.Users;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import model.*;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOPackager;
import org.jpos.iso.packager.GenericPackager;
import utility.ParseISOMessage;
import utility.Saldo;

/**
 *
 * @author kurnia
 */
public class BILLER implements Runnable{

  private static final Logger logger = Logger.getLogger("BILLER");
    Saldo mutasi = new Saldo();
    Connection conx = null;
    Connection conOtomax = null;
    Settings setting = new Settings();
    double pln_price = 0;
    double initial_nominal = 0;
    long curr_saldo = 0;
    int inbox_id = 0;
    int inbox_id_pelangi = 0;
    String serializeQueue = "";
    String[] arrayQueue = new String[8];
      
  public BILLER(String serializeQueue) {
        this.serializeQueue = serializeQueue;
        //arrayQueue = serializeQueue.split(";");

        ////////////////////the magic///////////////////////////////
        String msg = serializeQueue;
        logger.log(Level.INFO, "Msg init : " + msg);
        logger.log(Level.INFO, "Panjang msg : " + msg.split(";").length);
        String msgArray[] = new String[8];

        if (msg.split(";").length < 8) {
            String tempA[] = msg.split(";");
            msg = msg + tempA[0];
        }

        logger.log(Level.INFO, "Msg fin : " + msg);

        msgArray = msg.split(";");

        logger.log(Level.INFO, "number array : " + msgArray.length);
        for (int i = 0; i < msgArray.length; i++) {
            logger.log(Level.INFO, "msg[" + i + "] : " + msgArray[i]);
        }

        //rearrangement
        logger.log(Level.INFO, "#################################");
        logger.log(Level.INFO, "arr[0] : " + msgArray[0]);
        logger.log(Level.INFO, "arr[1] : " + msgArray[1]);
        msgArray[2] = msgArray[msgArray.length - 5];
        logger.log(Level.INFO, "arr[2] : " + msgArray[2]);
        msgArray[3] = msgArray[msgArray.length - 4];
        logger.log(Level.INFO, "arr[3] : " + msgArray[3]);
        msgArray[4] = msgArray[msgArray.length - 3];
        logger.log(Level.INFO, "arr[4] : " + msgArray[4]);
        msgArray[5] = msgArray[msgArray.length - 2];
        logger.log(Level.INFO, "arr[5] : " + msgArray[5]);
        msgArray[6] = msgArray[msgArray.length - 1];
        logger.log(Level.INFO, "arr[6] : " +msgArray[6] );
        
        
        //get the command
        logger.log(Level.INFO, ";" + msgArray[2] + " # " + msg.indexOf(";" + msgArray[2]));
        logger.log(Level.INFO, ";" + msgArray[0] + " # " + msg.indexOf(msgArray[0] + ";"));

//        String cmd = msg.substring(msg.indexOf(msgArray[0] + ";") + msgArray[0].length() + 1, msg.indexOf(";" + msgArray[2]));
        String cmd = msg.substring(msg.indexOf(msgArray[0] + ";") + msgArray[0].length() + 1);
        logger.log(Level.INFO, "command : " + cmd);
        msgArray[1] = cmd;

        arrayQueue = msgArray;

        ///////////////////////////////////////////the magic///////////////////////////////
        this.inbox_id = Integer.parseInt(arrayQueue[0]);
        String dts = arrayQueue[6].replaceAll("WEB","");
        int inboxid = Integer.parseInt(dts);
        System.out.println("data:::"+inboxid);
        this.inbox_id_pelangi = inboxid;
        
        

        setting.setConnections();
    }
    public void run() {
        process();
    }
    public synchronized void process() {
        int price_template_id = 0;

//        
         PostPaid post = null;//new PostPaid();
//        PostpaidResponseCode postRC = new PostpaidResponseCode();
        PostPaidProcessing postPro = new PostPaidProcessing();

        String hasil = "";
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        System.out.println("=========================================================================================");
        System.out.println(setting.getProduk());
        System.out.println(setting.getPersistenceMap().get("openjpa.ConnectionUserName"));
        System.out.println(setting.getPersistenceMap().get("openjpa.ConnectionPassword"));
        System.out.println(setting.getPersistenceMap().get("openjpa.ConnectionDriverName"));
        
        EntityManager em = factory.createEntityManager();

        String detail[] = new String[12];
        String product_detail[] = new String[10];
        int admin_charge_setting = 1600;

        RainbowCaller core = null;
        String id_pelanggan2 = "";
        try {

            core = new RainbowCaller();

            //inbox message
            detail[0] = arrayQueue[1];
            //inbox sender
            detail[1] = arrayQueue[2];
            //inbox media type id
            detail[2] = arrayQueue[3];
            //inbox id
            detail[3] = String.valueOf(inbox_id);
            //inbox receiver
            detail[4] = arrayQueue[4];
            //inbox user id
            detail[5] = arrayQueue[5];
            //inbox sender type
            detail[6] = arrayQueue[6];
//            detail[7] = arrayQueue[7]; // product id
            detail[8] = arrayQueue[1]; //harga

            detail[9] = "0"; //nominal admin charge
            detail[10] = ""; //curr saldo
//            detail[11] = arrayQueue[8]; //inbox id origin

//            int user_id=Integer.parseInt(detail[5]);
//            System.out.println("User ID:: "+user_id);
            detail[0] = arrayQueue[1];
             String codeProd[] = new String[9];
             codeProd = detail[0].split("\\.");
             
             
            String  data[]= new String [9];
            data= detail[0].split("\\.");
            System.out.println("data [2]:::"+data[1]);
            String prod=data[1].substring(0, 6);
            System.out.println("data Product::"+prod);
            detail[11]=data[2];
            
            //////////////////////////////////////////////////
            //get Product Detail
            String UserID=detail[5];
            String prodID="";
            Settings prodid =new Settings();
            if ("speedy".equalsIgnoreCase(prod)) {
                String id= prodid.getKodeProduk();
                prodID=id;
                
                System.out.println("speedy::"+id);
                
                 product_detail = this.getProductDetail(id);
                 String sss ="48";
            post = new PostPaid(Integer.parseInt(UserID));
                  admin_charge_setting = Integer.parseInt(product_detail[4]);
            logger.log(Level.INFO, "Admin charge setting : " + admin_charge_setting);
            } else {
                 String id= prodid.getKodeProduk1();
                  prodID=id;
                 System.out.println("Telkom::"+id);
                 product_detail = this.getProductDetail(id);
                 String sss ="48";
                post = new PostPaid(Integer.parseInt(UserID));
                  admin_charge_setting = Integer.parseInt(product_detail[4]);
                logger.log(Level.INFO, "Admin charge setting : " + admin_charge_setting);
            }
            int user_id= Integer.parseInt(prodID);
            detail[7]=prodID;

//            user_id=Integer.parseInt(detail[5]);
//            System.out.println("User ID:::"+user_id);
            
           
            //minus amount with nominal
            detail[9] = product_detail[4];
            logger.log(Level.INFO, "detail[9] : " + detail[9]);
            /////////////////////////////////////////////////

            // diganti iwan 20140411
            price_template_id = this.getPriceTemplateId(Integer.parseInt(detail[5]));

//            /////////////////UPDATE STATUS INBOX/////////////////
            setStatus(902,inbox_id);
//            
//            ////////////////get saldo/////////////////
            logger.log(Level.INFO, "Retrieving Saldo");
//
//            /// diganti biar cepetan dikit - iwan - 20140411
//           
            String theBalance = String.valueOf(this.getSaldo(Integer.parseInt(detail[5])));
//
            double balq = Double.parseDouble(theBalance);
            curr_saldo = (long) balq;
//
            logger.log(Level.INFO, "Saldo : " + curr_saldo);
//
//            ///////////////////////////////////////
//            ///////////////get limit EDC///////////////
            String limit_edc = "0";
//
            if (detail[6].equalsIgnoreCase("edc")) {
                limit_edc = this.getLImitEdc(Integer.parseInt(detail[5]));
            }
            logger.log(Level.INFO, "Limit EDC : " + limit_edc);

            //////////////end of get limit edc////////////////////
            logger.log(Level.INFO, "message : " + detail[0]);

            //Delivery Channel
            //String merchant_code = "6012"; //teller
            logger.log(Level.INFO, "Merchant : -" + detail[6] + "-");
            String merchant_code = "6012";

            if (detail[6].equalsIgnoreCase("teller")) {
                merchant_code = "6010";
            } else if (detail[6].equalsIgnoreCase("autodebet")) {
                merchant_code = "6013";
            } else if (detail[6].equalsIgnoreCase("edc")) {
                merchant_code = "6018";
            } else if (detail[6].equalsIgnoreCase("sms")) {
                merchant_code = "6023";
            } else if (detail[6].equalsIgnoreCase("mobile")) {
                merchant_code = "6023";
            } else if (detail[6].toLowerCase().equalsIgnoreCase("xmpp")) {
                merchant_code = "6012";
            }

            logger.log(Level.INFO, "Merchant Code : -" + merchant_code + "-");

            String terminal_id = String.format("%16s", detail[5]).replace(' ', '0');

            String trxFrom = "retail";

            //validate idpel
            String dataLoket = "";

            if (detail[0].contains("||")) {
                trxFrom = "hth";
                String[] dataLoketX = detail[0].split("\\|\\|");

                detail[0] = dataLoketX[0];
                dataLoket = dataLoketX[1].substring(5);
                if (dataLoketX[1].substring(0,5).toLowerCase() == "loket") {
                    dataLoket = "";
                }
            }

            String rex[] = detail[0].split("\\.");
            String id_pelanggan = rex[2].substring(2);
            String lembarBayar = rex[2].substring(0, 2);
            id_pelanggan2 = id_pelanggan;

            String  data1[]= new String [9];
            data1= detail[0].split("\\;");

            
            if (Pattern.matches("telkom.speedycek.[0-9]+", data1[0].toLowerCase())) {
                System.out.println("========TELKOM INQUIRY START CEK TELKOM==========");
                System.out.println("inbox id::"+inbox_id);
                String[] userData = new String[8];
                userData = getLoket(Integer.parseInt(detail[5]));

                hasil = post.request_inquiry2(detail[0], detail[5], String.valueOf(inbox_id), merchant_code, terminal_id, id_pelanggan2, lembarBayar, userData);

                System.out.println("masuk");
                 hasil = postPro.processInquiryBpjs(hasil, detail[3], user_id, dataLoket);

                System.out.println("========TELKOM XML : " + hasil + "==========");
                System.out.println("========TELKOM INQUIRY DONE==========");

            }else if(Pattern.matches("telkom.speedypay.[0-9]+", data1[0].toLowerCase())){
                System.out.println("========TELKOM PAYMENT START PAY TELKOM==========");
                System.out.println("inbox id::"+inbox_id);
                String[] userData = new String[8];
                String prehasil = post.request_inquiry2(detail[0], detail[5], String.valueOf(inbox_id), merchant_code, terminal_id, id_pelanggan2, lembarBayar, userData);
                
                if (prehasil.toLowerCase().contains("xml")) { //note: artinya inquiry gagal
                    System.out.println("IFFFFFFFFFF>>>>>:");
                    hasil = prehasil;

                    /////////////////UPDATE STATUS INBOX/////////////////
                    setStatus(901, inbox_id_pelangi);
                    /////////////////////////////////////////////////////
                } else {
                    System.out.println("ESLEEEE");
                    String[] result2 = post.parseInquiryMsgResponse(prehasil, false);
                    System.out.append("MSG : " + result2[13]);
                    System.out.append("RC : " + result2[10]);
                    ParseISOMessage isoMessage = new ParseISOMessage();
                    Map<String, String> bitMapNyo = null;
                    try {
                    bitMapNyo = isoMessage.getBitmap(prehasil);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (result2[10].equalsIgnoreCase("00")) {

                        String nominal = result2[2].substring(4, result2[2].length());
                        String[] reqPost = detail[0].split("\\.");

                        int jumlah_rek = 1;

                        try {
                            System.out.println(">>>>>>>>>>>>>>>+++++++Parse48");
                            //note: parsing berdasarkan respon inquiry yang diterima dari symphoni
//                            String msg, String rc
                            String[] bit48 = post.parseBit48InquiryTelco(result2[13], result2[10]);
                            System.out.println("AMOUNTTTT>>>>>::::"+bit48[6]);
                            String amount = bit48[6];
                            String hasilp = "";
                            String Amount="";
                            for(int i = 0; i < amount.length(); i++) {
                              hasilp = String.valueOf(amount.charAt(i));

                              if(Integer.parseInt(hasilp) > 0){
                                String admin = amount.substring(amount.indexOf(amount.charAt(i)));

                                 Amount=admin;

                                break;
                              }
    }           
                            System.out.println("AMOUNT::::::>>"+Amount);;
                           admin_charge_setting = Integer.parseInt(Amount);
                        } catch (Exception e) {
                            logger.log(Level.FATAL, e.toString());
                        }

                        /////////////nominal dikurangi admin///////////////
                        Long hargax = Long.parseLong(nominal);
                        nominal = hargax.toString();
                        ///////////////////////////////////////////////////
                        //kalau cukup saldonya                
//                        if (true) {
//                detail[0] = arrayQueue[1];
//                String codePr[] = new String[9];
//                codePr = detail[0].split("\\;");
    
                    String split[] = detail[0].split(";"); 
                        System.out.println("Hitung saldo");
                        System.out.println(""+Double.parseDouble(nominal));
                        System.out.println(""+Integer.parseInt(detail[3]));
                        System.out.println(""+UserID);
                        System.out.println(""+split[0].toLowerCase());
                        System.out.println(""+Integer.parseInt(prodID));
                        System.out.println(""+price_template_id);
                        System.out.println("Jumlah Rekening:"+jumlah_rek);
                        System.out.println("++++++++++++++++++++++++++++++++++");
//                        double nominal, 
//                        int inbox_id,
//                        int user_id, 
//                        String msg, 
//                        int product_id, 
//                        int price_template_id, 
//                        int jumlah_rekening
                        if (itung(Double.parseDouble(nominal), 
                                Integer.parseInt(detail[3]),
                                Integer.parseInt(UserID),
                                split[0].toLowerCase(),
                                Integer.parseInt(prodID),
                                price_template_id, jumlah_rek)) {
                                String[] userData1 = new String[8];
                                userData1 = getLoket(Integer.parseInt(UserID));
//String msg, String trx_id, String user_id,
//            String merchant_cat_code, String terminal_id, String hasilInquiry, int admin, int product_id, String[] userData
                            hasil = post.bill_payment_bpjs( split[0], String.valueOf(inbox_id), UserID,
                                    merchant_code, terminal_id, prehasil, admin_charge_setting,
                                    Integer.parseInt(prodID), userData1);
//                          
                            logger.log(Level.INFO, "hasil billpayment non edc : " + hasil);
                            logger.log(Level.INFO, "curr saldo : " + curr_saldo + " - pln price : " + pln_price);
                            detail[10] = String.valueOf(curr_saldo);
                            detail[8] = String.valueOf(pln_price);

                            hasil = postPro.processPaymentBpjs(hasil, String.valueOf(inbox_id), pln_price,
                                    detail, user_id, String.valueOf(inbox_id_pelangi), userData);
                            logger.log(Level.INFO, "hasil process payment : " + hasil);

                        } else {
                            hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                                    + "<response>"
                                    + "<stan>" + inbox_id_pelangi + "</stan>"
                                    + "<trx_id></trx_id>"
                                    + "<produk>PPOB TELKOM</produk>"
                                    + "<code>0046</code>"
                                    + "<desc>Saldo tidak cukup</desc>"
                                    + "<saldo>" + curr_saldo + "</saldo>"
                                    + "</response>";

                            /////////////////UPDATE STATUS INBOX/////////////////                                    
                            setStatus(400, inbox_id_pelangi);
                            /////////////////////////////////////////////////////
                        }

                    }//gangguan
                    else {

                        hasil = postPro.processInquiry(prehasil, detail);

                        /////////////////UPDATE STATUS INBOX/////////////////
                        setStatus(901, inbox_id_pelangi);
                        /////////////////////////////////////////////////////
                    }
                }
            }else if(Pattern.matches("telkom.telkomcek.[0-9]+", data1[0].toLowerCase())){
                System.out.println("TELKOM CEK");
            }else if(Pattern.matches("telkom.telkompay.[0-9]+", data1[0].toLowerCase())){
                System.out.println("TELKOM PAY");
            }
            logger.log(Level.INFO, "TRX TELKOM RESULT : " + hasil);
            
            hasil = hasil.replaceAll("\\P{Print}", "");

            //////cek hasil akhir ///////////////
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc;
            InputStream xmlAkhir = new ByteArrayInputStream(hasil.toString().getBytes());
            doc = docBuilder.parse(xmlAkhir);
            doc.getDocumentElement().normalize();
            logger.info(doc.getDocumentElement().getNodeName());

            String rc = "";
            try {
                NodeList nodeRc = doc.getElementsByTagName("code");
                Element eRc = (Element) nodeRc.item(0);
                NodeList rcx = eRc.getChildNodes();

                rc = ((Node) rcx.item(0)).getNodeValue().trim();

                if (!rc.equalsIgnoreCase("0000")) {
                    //setInboxStatus = core.call("status.901." + detail[3]);
                    setStatus(901, inbox_id_pelangi);
                }

            } catch (Exception e) {
                rc = "0000";
                logger.log(Level.FATAL, "CODE ra ono : " + e.getMessage());
                //setInboxStatus = core.call("status.200." + detail[3]);
                setStatus(200, inbox_id_pelangi);
            }
            //////cek hasil akhir ///////////////

            if (hasil.length() > 0) {
                //INSERT TO OUTBOXES
                System.out.println("INSERT TO OUTBOXES");
                insertToOutbox(hasil, detail[1],
                        detail[6], "" + inbox_id_pelangi, Integer.parseInt(detail[5]),
                        detail[4], Integer.parseInt(detail[2]), rc, id_pelanggan);
            }

            
            core.close();

        } catch (Exception e) {
            System.out.println("Catch Prosess>>>>>>>>>>..");
            e.printStackTrace();
            logger.log(Level.FATAL, e.toString());

            try {
                core.close();
            } catch (Exception ex) {
                System.out.println("catch closee CORE>>>>>>...");
            }

            hasil = "<?xml version='1.0' encoding='UTF-8'?>"
                    + "<response>"
                    + "<stan>" + String.valueOf(inbox_id_pelangi) + "</stan>"
                    + "<produk>TELKOM</produk>"
                    + "<code>0005</code>"
                    + "<desc>SYSTEM ERROR</desc>"
                    + "<saldo>" + curr_saldo + "</saldo>"
                    + "</response>";

        } 
    }

    public void insertToOutbox(String msg, String receiver, String receiver_type, String transaction_id,
            int user_id, String sender, int media_type_id, String response_code, String id_pelanggan) {

        msg = msg.replace("<response>", "<response><stan_cycle>" + inbox_id + "</stan_cycle>");
        boolean isSMS = false;
        int status = 1;
        if (media_type_id != 1) {
            insertToQueue(msg, receiver, receiver_type, transaction_id,
                    user_id, sender, media_type_id, response_code, id_pelanggan);
        } else if (media_type_id == 1) {
            isSMS = true;
            status = 0;
        }

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();

        try {
            Outboxes outbox = new Outboxes();
            em.getTransaction().begin();

            outbox.setMessage(msg);
            outbox.setStatus(status);
            outbox.setCreateDate(new java.util.Date());
            outbox.setReceiver(receiver);
            outbox.setReceiverType(receiver_type);

            outbox.setTransactionId(inbox_id_pelangi);
            outbox.setInboxId(inbox_id_pelangi);
            outbox.setUserId(user_id);
            outbox.setSender(sender);
            outbox.setResponseCode(response_code);
            outbox.setMediaTypeId(media_type_id);
            outbox.setSms(isSMS);
            em.persist(outbox);
            em.getTransaction().commit();

            em.close();
            factory.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage());

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }
    }
     //return To API
    private void insertToQueue(String msg, String receiver, String receiver_type, String transaction_id, int user_id, String sender, int media_type_id, String response_code, String id_pelanggan) {
;

        String QUEUE_NAME = "outbox";
        String TASK_QUEUE_NAME = "outbox_queue";
        String EXCHANGE_NAME = "outbox_general_exchange";
        String ROUTING_KEY = "outbox.general";

        try {

            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost(setting.getRabbitHost());
            com.rabbitmq.client.Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();

            BillerConverter plnConverter = new BillerConverter();
            String message = receiver + "#" + msg;
            String isXml = "";
            if (sender.contains("NONXML")) {
                message = receiver + "#" + plnConverter.convertPrepaid(msg, id_pelanggan);
                isXml = "NONXML";
            } else {


                QUEUE_NAME = setting.getRabbit_hostOut();
                TASK_QUEUE_NAME = setting.getRabbit_queueNameOut();
                EXCHANGE_NAME = setting.getRabbit_exchangeOut();
                ROUTING_KEY = setting.getRabbit_bindingKeyOut();
                
                isXml = "XML";
            }

            message += "#" + media_type_id;

            channel.exchangeDeclare(EXCHANGE_NAME, "topic", true);
            channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);

            if (!message.equalsIgnoreCase("0")) {

                channel.basicPublish(EXCHANGE_NAME, ROUTING_KEY,
                        MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes());

                //System.out.println(" [x] Sent '" + message + "'");
                logger.log(Level.INFO, " [x] Sent '" + message + "'  <==>  " + sender + " <==> " + receiver + " <==> " + isXml);
            }

            channel.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e);
        }
    }

        public String[] getProductDetail(String product_code) {
            System.out.println("Get Product DEtaillll:::::::"+product_code);
            
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();

        String[] output = new String[9];

        try {
            String sql = "SELECT o FROM Products o "
                    + " where o.id =:code";
            Query query = em.createQuery(sql);
           query.setParameter("code", Long.parseLong(product_code));
            query.setMaxResults(1);

            for (Products m : (List<Products>) query.getResultList()) {
                output[0] = String.valueOf(m.getId());
                output[1] = m.getCode();
                output[2] = m.getName();
                output[3] = m.getDescription();
                output[4] = String.valueOf(m.getNominal());
                output[5] = "0000";
                output[6] = String.valueOf(m.getActive());
                output[7] = String.valueOf(m.getKosong());
                output[8] = String.valueOf(m.getGangguan());

            }
            System.out.println("AMOOUNT"+output[4]);

        } catch (Exception e) {
            output[3] = "Deskripsi tidak tersedia";
            output[5] = "0005";
            logger.log(Level.FATAL, e.getMessage());
        } finally {
            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }
        }

        return output;
    }
        public int getPriceTemplateId(int user_id) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();

        int price_template_id = 0;
        System.out.println("User id : " + user_id);
        try {
            System.out.println("tempalat1111");
            Users user = em.find(Users.class, user_id);
            System.out.println("tempalat11112222222222222222");
            price_template_id = user.getPriceTemplateId();

            em.close();
            factory.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.toString());
        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }

        return price_template_id;
    }
        public boolean setStatus(int code, int id) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();

        boolean result = false;
        System.out.print("Set status>>");
        try {

            em.getTransaction().begin();
            Query query = em.createQuery("UPDATE Inboxes o SET o.status = :stat where o.id=:inbox_id");
            query.setParameter("stat", code);
            int x = query.setParameter("inbox_id", id).executeUpdate();

            System.out.println("update : " + x);
            em.getTransaction().commit();

            em.close();
            factory.close();

            result = true;
            System.out.println("<<set status ok");
        } catch (Exception e) {
            result = false;
            e.printStackTrace();
            logger.log(Level.FATAL, e.getMessage() + " " + e.toString());

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

            result = setStatus(code, id);
        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }

        return result;
    }
     public long getSaldo(int user_id) {

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();
        long saldo = 0;

        try {

            //checking balance
            String sql = "select SUM(m.amount) from Mutations m where m.userId=:uid";
            Query qq = em.createQuery(sql);
            qq.setParameter("uid", user_id);

            Number saldoq = (Number) qq.getSingleResult();

            saldo = saldoq.longValue();

            em.close();
            factory.close();

        } catch (Exception e) {

            logger.log(Level.FATAL, e.getMessage() + " " + e.toString());

            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }
        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }

        return saldo;
    }
     public String getLImitEdc(int user_id) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();

        String limit_edc = "";
        System.out.println("User id : " + user_id);
        try {

            Users user = em.find(Users.class, user_id);
            limit_edc = user.getLimitEdc();

            em.close();
            factory.close();

            if (limit_edc == null) {
                limit_edc = "0";
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.toString());
        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }

        return limit_edc;
    }
     public String[] getLoket(int user_id) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();

        String[] userData = new String[8];
        System.out.println("User id : " + user_id);
        try {

            Users user = em.find(Users.class, user_id);
            userData[0] = user.getUsername();
            userData[1] = user.getCompany();
            userData[2] = user.getAddress();
            userData[3] = user.getPhone();
            userData[4] = user.getFirstName();
            userData[5] = user.getKabKota().toUpperCase();
            userData[6] = user.getKodeKabKota().toUpperCase();
            userData[7] = user.getKodePos().toUpperCase();

            if (userData[1].length() > 30) {
                userData[1] = userData[1].substring(0, 30);
            }
            if (userData[2].length() > 50) {
                userData[2] = userData[2].substring(0, 50);
            }
            if (userData[3].length() > 18) {
                userData[3] = userData[3].substring(0, 18);
            }

            if (userData[5].length() > 30) {
                userData[5] = userData[5].substring(0, 10);
            }

            em.close();
            factory.close();

        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e.toString());
        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }

        return userData;
    }
     public String rebuildISO(int inbox_id) {

        String iso = "";
        String xml = "";
        String stan = "",idpel="" ,rc = "", dt = "", terminal = "", produk = "", bit48 = "", bit62 = "", bank_code = "", merchant_code = "", pan = "", bit4 = "0";
        
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("SLS", setting.getPersistenceMap());
        EntityManager em = factory.createEntityManager();
        
        inbox_id=208957973;
        try {
            
            //checking balance
            String sql = "select m from Outboxes m where m.inboxId=:uid";
            Query qq = em.createQuery(sql);
            qq.setParameter("uid", inbox_id);
            qq.setMaxResults(1);

            for (Outboxes m : (List<Outboxes>) qq.getResultList()) {
                xml = m.getMessage();
            }

            logger.log(Level.INFO, "REBUILD ISO FOR XML : " + xml);
            iso = xml;

            //parse the xml
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc;
            InputStream xmlAkhir = new ByteArrayInputStream(xml.toString().getBytes());

            doc = docBuilder.parse(xmlAkhir);
            
            System.out.println("doc ; "+doc);
            doc.getDocumentElement().normalize();
            Map<String, String> tagMap = new HashMap<String, String>();
            System.out.println("doc ; "+doc.getElementsByTagName("rc"));
            
            try {
                System.out.println("MSUKKKKKK");
                NodeList nodeRc = doc.getElementsByTagName("rc");
                Element eRc = (Element) nodeRc.item(0);
                NodeList rcx = eRc.getChildNodes();
                rc = ((Node) rcx.item(0)).getNodeValue().trim();

                logger.log(Level.INFO, "RC : " + rc);
                
                
//                  NodeList nodeIdpel = doc.getElementsByTagName("rc");
//                Element eIdpel = (Element) nodeIdpel.item(0);
//                System.out.println("eIdpel :::::::::::::::::::: "+eIdpel);
//
//                NodeList idpelx = eIdpel.getChildNodes();
//                System.out.println("idpelx :::::::::::::::::::: "+idpelx);
//
//                idpel = ((Node) idpelx.item(0)).getNodeValue().trim();
//                System.out.println("idpel :::::::::::::::::::: "+idpel);
//                System.exit(0);
                
                if (rc.equalsIgnoreCase("00")) {

                    String[] tagList = {"trx_type", "product_type", "merchant_type", "bank_code", "terminal_id", "product_code", "amount", "stan", "date_time", "rc", "no_va", "jumlah_bulan", "nama", "kode_cabang", "nama_cabang", "biaya_premi", "biaya_admin", "sisa","private_data_48"};
                    for (int i = 0; i < tagList.length; i++) {
                        String tagList1 = tagList[i];
                        NodeList nodeTag = doc.getElementsByTagName(tagList[i]);
                        Element eTag = (Element) nodeTag.item(0);
                        NodeList tagx = eTag.getChildNodes();
                        tagMap.put(tagList[i], ((Node) tagx.item(0)).getNodeValue());
                        logger.log(Level.INFO, tagList[i] + " : " + tagMap.get(tagList[i]));
                    }
                    //build the ISO 
                    // membuat sebuah packager
                    ISOPackager packager = new GenericPackager("packager/iso87ascii.xml");
                    ISOMsg isoMsg = new ISOMsg();

                    Settings setting = new Settings();
                    setting.setConnections();
                    String cid = setting.getSwitchingCID();

                    bit48 = tagMap.get("private_data_48");
               // Create ISO Message
                    isoMsg.setPackager(packager);
                    isoMsg.setMTI("0210");
                    isoMsg.set(2, "064009");
                    isoMsg.set(4, "000000272018");
                    isoMsg.set(7, "0619145207");
//                    isoMsg.set(11, tagMap.get("stan"));
                    isoMsg.set(11, "281365");
                    isoMsg.set(12, "145207");
                    isoMsg.set(13, "0619");
                    isoMsg.set(15, "0620");
//                    isoMsg.set(26, tagMap.get("merchant_type"));
                    isoMsg.set(32, "008");
                    isoMsg.set(37, "000000281365");
                    isoMsg.set(39, tagMap.get("rc"));
                    isoMsg.set(41, tagMap.get("terminal_id"));
                    isoMsg.set(48, bit48);
                    isoMsg.set(49, "360");
                    isoMsg.set(61, "00210788401090200041906A             269018 SUPRIYATI                                   ");
                            byte[] datax = isoMsg.pack();
                    logger.log(Level.INFO, "The rebuilt ISO : " + new String(datax));
                    iso = new String(datax);
                }

            } catch (Exception e) {
                e.printStackTrace();
                logger.log(Level.INFO, e.toString());
            }

            em.close();
            factory.close();

        } catch (Exception e) {

            if (em.getTransaction().isActive()) {
                em.getTransaction().rollback();
            }

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }
        } finally {

            if (em.isOpen()) {
                em.close();
            }

            if (factory.isOpen()) {
                factory.close();
            }

        }

        return iso;
    }
     public boolean itung(double nominal, int inbox_id, int user_id, String msg, int product_id, int price_template_id, int jumlah_rekening) {

        boolean boleh = false;
        RainbowCaller core = null;

        try {
            core = new RainbowCaller();

            logger.log(Level.INFO, "itung." + (int) nominal + "." + inbox_id + "." + user_id + "." + "SPEEDY" + "." + product_id + "." + price_template_id + "." + jumlah_rekening);
            String itungan = core.call("itung." + (int) nominal + "." + inbox_id + "." + user_id + "." + "SPEEDY" + "." + product_id + "." + price_template_id + "." + jumlah_rekening);
            logger.log(Level.INFO, "Hasil itungan : " + itungan);
            String rex[] = itungan.split("\\|");
            logger.log(Level.INFO, "hasil : " + rex[0]);
            if (rex[0].equalsIgnoreCase("true")) {
                boleh = true;
                pln_price = Double.parseDouble(rex[1]);
                logger.log(Level.INFO, "price : " + rex[1]);
                curr_saldo = Long.parseLong(rex[2]);
                logger.log(Level.INFO, "saldo : " + rex[2]);
            }

            core.close();

        } catch (Exception e) {
            boleh = false;
            logger.log(Level.FATAL, e.getMessage() + " < " + inbox_id + " > " + e.toString());
            try {
                core.close();
            } catch (Exception ex) {
            }

        } finally {
        }

        return boleh;
    }


}
